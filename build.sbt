val scala3Version = "3.3.1"

lazy val root = project
  .in(file("."))
  .settings(
        name := "reswing",
        version := "0.9.0",
        organization := "fr.pointfixe",

        scalaVersion := scala3Version,

        libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test withSources() withJavadoc(),

        scalacOptions ++= Seq(
              "-encoding", "utf8",
              //"-explaintypes",
              "-Ysafe-init",
              "-Yexplicit-nulls",
              //  "-language:unsafeNulls",
        )
  )
