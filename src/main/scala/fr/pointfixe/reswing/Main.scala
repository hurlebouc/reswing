package fr.pointfixe.reswing

import fr.pointfixe.reswing.component.{Button, Frame, HBox, Menu, MenuItem, Panel, Representation, TextField, Tree}
import fr.pointfixe.reswing.runtime.Cloak

import java.awt.BorderLayout
import javax.swing.{GroupLayout, JButton, JFrame, JMenu, JMenuBar, JMenuItem, JPanel, JSplitPane, JTree, KeyStroke, WindowConstants}

object Main {

  private def counterButton(): RepresentationFactory[String] = new StatefullRepresentationFactory[String] {

    val (getCounter, setCounter) = useState(1)

    override def build(param: String)(using redraw: () => Unit): Representation[?] = Button(
      text = f"$param ${getCounter()}",
      onClick = () => {
        setCounter(getCounter() + 1)
      }
    )
  }

  private val nestedState: RepresentationFactory[Unit] = new StatefullRepresentationFactory[Unit] {

    private val counterButton1 = counterButton()
    private val counterButton2 = counterButton()

    val (getText, setText) = useState("coucou")
    val (getBool, setBool) = useState(true)

    override def build(param: Unit)(using redraw: () => Unit): Representation[?] = {
      Frame(
        components = Seq(
          Panel(
            children = Seq(
              TextField(
                text = getText(),
                onTextChange = s => setText(s),
              ),
              Button(
                text = if getBool() then "turn off" else "turn on",
                onClick = () => setBool(!getBool()),
              ),
              counterButton1.build("plip - " + getText()),
              if getBool() then counterButton2.build("plop - " + getText()) else TextField(text = "plop - " + getText()),
            )
          )
        ),
        menus = Seq(
          Menu(
            text = "Fichier",
            items = Seq(
              MenuItem(
                text = "Ouvrir",
              ),
              MenuItem(
                text = "Fermer",
              ),
            )
          ),
          Menu(
            text = "Édition",
            items = Seq(
              MenuItem(
                text = "Annuler"
              ),
              MenuItem (
                text = "Rétablir"
              ),
            ),
          ),
        )
      )
    }
  }

  private val tree = new StatefullRepresentationFactory[Unit]{
    override def build(param: Unit)(using redraw: () => Unit): Representation[_] = Frame(
      components = Seq(
        Tree()
      )
    )
  }

  private val boxes = new StatefullRepresentationFactory[Unit]{
    override def build(param: Unit)(using redraw: () => Unit): Representation[_] = Frame(
      components = Seq(
        HBox(
          children = Seq(
            Button(text = "bouton 1"),
            Button(text = "bouton 2"),
          ),
        ),
      )
    )
  }

  @main def nestedStateRun() : Unit = {
    Cloak(nestedState).display()
  }

  @main def treeRun(): Unit = {
    Cloak(tree).display()
  }

  @main def boxesRun(): Unit = {
    Cloak(boxes).display()
  }

  @main def helloOld(): Unit = {
    val menuitem = new JMenuItem()
    menuitem.setText("coucou")
    menuitem.setAccelerator(KeyStroke.getKeyStroke("ctrl O"))
    menuitem.addActionListener(e => println("coucou"))
    val menu = new JMenu()
    menu.setText("Plop")
    menu.add(menuitem)
    val menubar = new JMenuBar()
    menubar.add(menu)
    val jFrame = new JFrame()
    jFrame.setJMenuBar(menubar)
    jFrame.setTitle("ReSwing")
    jFrame.setSize(800, 600)
    val contentPane = jFrame.getContentPane.asInstanceOf[JPanel]
    jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

    contentPane.add(new JTree())
    jFrame.setVisible(true)
    //splitPane.setDividerLocation(0.5)


    println("coucou")
  }

  @main def groupLayout(): Unit = {
    import scala.language.unsafeNulls
    val jFrame = new JFrame()
    jFrame.setTitle("ReSwing")
    jFrame.setSize(800, 600)
    val contentPane = jFrame.getContentPane.asInstanceOf[JPanel]
    jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

    val b1 = new JButton("bouton 1")
    val b2 = new JButton("bouton 2")
    val b3 = new JButton("bouton 3")
    val panel = new JPanel()
    val layout = new GroupLayout(panel)
    panel.setLayout(layout)
    contentPane.add(panel)
    layout.setHorizontalGroup(
      layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(b1)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(b3, 0, 0, Int.MaxValue)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Int.MaxValue)
        .addComponent(b2)
        .addContainerGap()
    )
    layout.setVerticalGroup(
      layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(
          layout.createParallelGroup()
          .addComponent(b1)
          .addComponent(b3)
          .addComponent(b2)
        )
        .addContainerGap()
    )

    jFrame.setVisible(true)
    //splitPane.setDividerLocation(0.5)


    println("coucou")
  }

  @main def requesterRun(): Unit = {
    Cloak(new Requester()).display()
  }

}
