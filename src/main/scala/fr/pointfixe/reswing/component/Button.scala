package fr.pointfixe.reswing.component

import javax.swing.JButton

case class Button(
              text : String,
              enabled : Boolean = true,
              onClick : () => Unit = () => {},
              constraints : AnyRef | Null = null,
            )
{
  private val parent = new AbstractButton(
    text = text,
    onClick = onClick,
    enabled = enabled,
    constraints = constraints,
  )
}

object Button {

  def apply(text: String, enabled: Boolean = true, onClick: () => Unit = () => {}, constraints: AnyRef | Null = null): Representation[Button] = Representation(new Button(text, enabled, onClick, constraints))

  given configuration : Configurable[Button, JButton] = Configurable.parentConfigurable(_.parent)

  given renderTo : RenderTo[Button, JButton] with {
    override def getConstraints(repr: Button): AnyRef | Null = repr.constraints

    override def newSwing(): JButton = new JButton()
  }
}