package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.Graphic

import javax.swing.{GroupLayout, JPanel}

case class HBox(
                 children: Seq[Representation[?]],
                 constraints : AnyRef | Null = null,
               )

object HBox {

  enum Constraint {
    case StickLeft, StickRight, StickBoth
  }

  def apply(
             children: Seq[Representation[?]],
             constraints: AnyRef | Null = null,
           ): Representation[HBox] = Representation(new HBox(children,
    constraints
  ))

  private def createSeqGroup(layout : GroupLayout, children : Seq[Graphic]) = {
    import scala.language.unsafeNulls
    val seqGroup = layout.createSequentialGroup()
    seqGroup.addContainerGap()
    children.zipWithIndex.foreach((child, index) => {
      if(isExtensible(children, index)){
        seqGroup.addComponent(child.component, 0, 0, Int.MaxValue)
      } else {
        seqGroup.addComponent(child.component)
      }
//      seqGroup.addComponent(child.component).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    })
    seqGroup.addContainerGap()
    //todo
    seqGroup
  }

  private def isExtensible(graphics : Seq[Graphic], index : Int) : Boolean = {
    isStickBoth(graphics(index)) && graphics.take(index).forall(g => isStickLeft(g) || isStickBoth(g)) && graphics.drop(index + 1).forall(g => isStickRight(g) || isStickBoth(g))
  }

  private def isStickLeft(graphic: Graphic) : Boolean = {
    graphic.constraints match
      case constraint : Constraint => {
        constraint match
          case Constraint.StickLeft => true
          case Constraint.StickRight => false
          case Constraint.StickBoth => false
      }
      case _ => false

  }

  private def isStickRight(graphic: Graphic) : Boolean = {
    graphic.constraints match
      case constraint : Constraint => {
        constraint match
          case Constraint.StickLeft => false
          case Constraint.StickRight => true
          case Constraint.StickBoth => false
      }
      case _ => false
  }

  private def isStickBoth(graphic: Graphic) : Boolean = {
    graphic.constraints match
      case constraint : Constraint => {
        constraint match
          case Constraint.StickLeft => false
          case Constraint.StickRight => false
          case Constraint.StickBoth => true
      }
      case _ => false
  }

  private def createParGroup(layout: GroupLayout, children: IterableOnce[Graphic]) = {
    import scala.language.unsafeNulls
    val seqGroup = layout.createParallelGroup()
    children.iterator.foreach(child => seqGroup.addComponent(child.component))
    seqGroup
  }

  given configure : Configurable[HBox, JPanel] with {
    override def configure(repr: HBox, swing: JPanel): Unit = {
      import scala.language.unsafeNulls
      val layout = new GroupLayout(swing)
      swing.setLayout(layout)
      val comps = repr.children.map(c => c.render())
      layout.setHorizontalGroup(
        createSeqGroup(layout, comps)
      )
      layout.setVerticalGroup(
        createParGroup(layout, comps)
      )
    }

    override def merge(swing: JPanel, oldRepr: HBox, newRepr: HBox): Unit = ???
  }

  given RenderTo[HBox, JPanel] with {
    override def getConstraints(repr: HBox): AnyRef | Null = repr.constraints

    override def newSwing(): JPanel = new JPanel()
  }
}
