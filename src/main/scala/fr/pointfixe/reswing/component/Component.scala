package fr.pointfixe.reswing.component

import fr.pointfixe.reswing.layout.LayoutManager

import java.awt
import javax.swing.JComponent

case class Component (
                           layoutManager: LayoutManager | Null = null,
                           constraints: AnyRef | Null = null,
                           enabled: Boolean,
                           children: Seq[Representation[?]] | Null = null
                         )
{
  private val parent = new Container(
    layoutManager = layoutManager,
    enabled = enabled,
    constraints = constraints,
    children = children,
  )
}

object Component {
  given configuration : Configurable[Component, JComponent] = Configurable.parentConfigurable(_.parent)
}
