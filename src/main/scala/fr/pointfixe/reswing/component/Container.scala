package fr.pointfixe.reswing.component

import fr.pointfixe.reswing.layout.LayoutManager
import fr.pointfixe.reswingOld.Graphic

import java.awt.Container as WContainer
import java.awt.Component as WComponent

case class Container (
                           layoutManager: LayoutManager | Null = null,
                           constraints: AnyRef | Null = null,
                           enabled: Boolean,
                           children: Seq[Representation[?]] | Null = null, // le null signifie : ce champs n'est pas géré par Reswing
                )

object Container {
  given configuration : Configurable[Container, WContainer] with {

    def configure(repr: Container, swing : WContainer) : Unit = {
      swing.setEnabled(repr.enabled)
      if (repr.layoutManager != null) {
        swing.setLayout(repr.layoutManager.getSwing(swing))
      }
      if (repr.children != null) {
        for ((child, i) <- repr.children.zipWithIndex) {
          val childGraphic = child.render()
          if(childGraphic.constraints == null){
            swing.add(childGraphic.component, i)
          } else {
            swing.add(childGraphic.component, childGraphic.constraints, i)
          }
        }
      }
    }

    def merge(swing: WContainer, oldRepr: Container, newRepr: Container): Unit = {
      swing.setEnabled(newRepr.enabled)
      if (newRepr.layoutManager != null) {
        swing.setLayout(newRepr.layoutManager.getSwing(swing))
      }
      resetChildren(Children.of(swing), oldRepr.children, newRepr.children)
    }
  }

  def resetChildren(swing: Children, oldChildren: Seq[Representation[?]] | Null, newChildren: Seq[Representation[?]] | Null): Unit = {
    if (oldChildren == null) {
      if (newChildren != null) {
        swing.removeAll() // on ne gérait pas, maintenant on gère
        for ((newChild, index) <- newChildren.zipWithIndex) {
          val childGraphic = newChild.render()
          if (childGraphic.constraints == null) {
            swing.add(childGraphic.component, index)
          } else {
            swing.add(childGraphic.component, childGraphic.constraints, index)
          }
        }
      }
    } else {
      val current = oldChildren.zip(swing.getComponents())
      if (newChildren == null) {
        for (i <- oldChildren.indices) {
          swing.remove(0)
        }
      } else {
        val mergeList = current.zip(newChildren).map((curr, newChild) => (curr._1, curr._2.nn, newChild))
        val newChildrenOther = mergeList.map((oldChild, comp, newChild) => oldChild.mergeGraphicWith(Graphic(comp, null), newChild))
        val oldNewComp = swing.getComponents().zip(newChildrenOther)
        for (i <- oldNewComp.indices) {
          val (oldComp, Graphic(newComp, const)) = oldNewComp(i)
          if (oldComp != newComp) {
            swing.remove(i)
            if (const == null) {
              swing.add(newComp, i)
            } else {
              swing.add(newComp, const, i)
            }
          }
        }
      }
    }
  }

  def resetChild(getChild : () => WComponent | Null,
                 setChild : WComponent | Null => Unit,
                 setChildWithContraint : (WComponent, AnyRef) => Unit,
                 oldChildRepr: Representation[?] | Null,
                 newChildRepr: Representation[?] | Null): Unit = {

    if (newChildRepr == null) {
      setChild(null)
    } else if (oldChildRepr == null) {
      val graphic = newChildRepr.render()
      if (graphic.constraints == null) {
        setChild(graphic.component)
      } else {
        setChildWithContraint(graphic.component, graphic.constraints)
      }
    } else {
      val oldComp = getChild().nn // non null car provient de oldChildRepr qui n'est pas null
      val Graphic(newComp, newConst) = oldChildRepr.mergeGraphicWith(Graphic(oldComp, null), newChildRepr)
      if (oldComp != newComp) {
        if (newConst == null) {
          setChild(newComp)
        } else {
          setChildWithContraint(newComp, newConst)
        }
      }
    }
  }
}
