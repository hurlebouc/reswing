package fr.pointfixe.reswing.component

import javax.swing.JLabel

case class Label(
                  text : String,
                  constraints : AnyRef | Null = null,
                  enabled : Boolean = true,
                )
{
  private val parent = new  Component(
    enabled = enabled,
    constraints = constraints,
  )
}

object Label {

  def apply(text: String, constraints: AnyRef | Null = null, enabled: Boolean = true): Representation[Label] = Representation(new Label(text, constraints, enabled))


  private def configure(repr: Label, swing: JLabel): Unit = {
    swing.setText(repr.text)
  }

  given configuration : Configurable[Label, JLabel] = Configurable.simpleConfigurable(_.parent, configure)

  given renderTo : RenderTo[Label, JLabel] with {
    override def getConstraints(repr: Label): AnyRef | Null = repr.constraints

    override def newSwing(): JLabel = new JLabel()
  }
}
