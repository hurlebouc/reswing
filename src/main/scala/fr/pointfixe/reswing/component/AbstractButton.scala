package fr.pointfixe.reswing.component

import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.AbstractButton as JAbstractButton

case class AbstractButton(
                           text : String,
                           onClick : () => Unit,
                           constraints : AnyRef | Null = null,
                           enabled : Boolean = true,
                           children: Seq[Representation[?]] | Null = null
                         )
{
  private val parent = new Component(
    enabled = enabled,
    constraints = constraints,
    children = children,
  )
}


object AbstractButton {

  private case class OnClickListener(repr : AbstractButton) extends ActionListener {
    override def actionPerformed(e: ActionEvent): Unit = repr.onClick()
  }

  given configuration : Configurable[AbstractButton, JAbstractButton] = Configurable.simpleConfigurable(_.parent, configure)

  private def configure(repr: AbstractButton, swing: JAbstractButton): Unit = {
    swing.getActionListeners.nn.filter(item => item.isInstanceOf[OnClickListener]).foreach(swing.removeActionListener)
    swing.setText(repr.text)
    swing.addActionListener(OnClickListener(repr))
  }
}
