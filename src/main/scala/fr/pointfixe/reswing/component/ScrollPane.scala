package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.{RenderCtx, Widget}

import javax.swing.JScrollPane

case class ScrollPane(
                       child : Representation[?],
                       enabled: Boolean = true,
                       constraints: AnyRef | Null = null,
                     )
{
  private val parent = new Component(
    constraints = constraints,
    enabled = enabled,
  )
}

object ScrollPane {

  def apply(
             child : Representation[?],
             enabled: Boolean = true,
             constraints: AnyRef | Null = null,
           ): Representation[ScrollPane] = Representation(new ScrollPane(child,
    enabled,
    constraints,
  ))

  given configure : Configurable[ScrollPane, JScrollPane] with {
    override def configure(repr: ScrollPane, swing: JScrollPane): Unit = {
      Component.configuration.configure(repr.parent, swing)
      swing.getViewport.nn.setView(repr.child.render().component)
    }

    override def merge(swing: JScrollPane, oldRepr: ScrollPane, newRepr: ScrollPane): Unit = {
      Component.configuration.merge(swing, oldRepr.parent, newRepr.parent)
      Container.resetChild(
        getChild = () => swing.getViewport.nn.getView(),
        setChild = comp => swing.getViewport.nn.setView(comp),
        setChildWithContraint = (compo, constraint) => swing.getViewport.nn.setView(compo),
        oldChildRepr = oldRepr.child,
        newChildRepr = newRepr.child,
      )
    }
  }

  given RenderTo[ScrollPane, JScrollPane] with {
    override def getConstraints(repr: ScrollPane): AnyRef | Null = repr.constraints

    override def newSwing(): JScrollPane = new JScrollPane()
  }
}