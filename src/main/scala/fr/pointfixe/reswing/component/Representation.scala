package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.Graphic

case class Representation[RE : Representable](repr : RE) {

  private val representable = summon[Representable[RE]]

  def render(): Graphic = representable.render2(repr)

  def mergeGraphicWith(graphic: Graphic, newRepr: Representation[?]): Graphic =
    representable.mergeGraphicWith2(graphic, repr, newRepr.repr)(using newRepr.representable)
}