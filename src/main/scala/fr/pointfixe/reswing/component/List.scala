package fr.pointfixe.reswing.component

import java.util
import javax.swing.JList

case class List[E](
                    items : Seq[E],
                    enabled: Boolean = true,
                    constraints: AnyRef | Null = null,
                  )
{
  private val parent = new Component(
    constraints = constraints,
    enabled = enabled,
  )
}

object List {

  def apply[E](items: Seq[E], enabled: Boolean = true, constraints: AnyRef | Null = null): Representation[List[E]] = Representation(new List(items, enabled, constraints))

  private def configure[E](repr: List[E], swing: JList[E]): Unit = {
    import scala.jdk.CollectionConverters.*
    swing.setListData(new util.Vector[E](repr.items.asJava))
  }

    given configuration[E] : Configurable[List[E], JList[E]] = Configurable.simpleConfigurable(_.parent, configure)

  given renderTo[E] : RenderTo[List[E], JList[E]] with {
    override def getConstraints(repr: List[E]): AnyRef | Null = repr.constraints

    override def newSwing(): JList[E] = new JList[E]()
  }
}
