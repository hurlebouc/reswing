package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.{RenderCtx, Widget}

import java.beans.{PropertyChangeEvent, PropertyChangeListener}
import javax.swing.JSplitPane

case class SplitPane(
                      dividerLocation: Int = 150,
                      onDividerLocationChange : Int => Unit = n => {},
                      left: Representation[?],
                      right: Representation[?],
                      constraints : AnyRef | Null = null,
                      enabled: Boolean = true,
                    )
{
  private val parent = new  Component(
    enabled = enabled,
    constraints = constraints,
  )
}

object SplitPane {

  def apply(dividerLocation: Int = 150,
            onDividerLocationChange : Int => Unit = n => {},
            left: Representation[?],
            right: Representation[?],
            constraints: AnyRef | Null = null,
            enabled: Boolean = true): Representation[SplitPane] = Representation(new SplitPane(dividerLocation,
    onDividerLocationChange,
    left,
    right,
    constraints,
    enabled))

  private case class OnDividerChange(repr : SplitPane) extends PropertyChangeListener {
    override def propertyChange(evt: PropertyChangeEvent): Unit = repr.onDividerLocationChange(evt.getNewValue.asInstanceOf)
  }

  given configuration : Configurable[SplitPane, JSplitPane] with {
    override def configure(repr: SplitPane, swing: JSplitPane): Unit = {
      Component.configuration.configure(repr.parent, swing)
      swing.setDividerLocation(repr.dividerLocation)
      swing.setLeftComponent(repr.left.render().component)
      swing.setRightComponent(repr.right.render().component)
      swing.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, OnDividerChange(repr))
    }

    override def merge(swing: JSplitPane, oldRepr: SplitPane, newRepr: SplitPane): Unit = {
      swing.getPropertyChangeListeners(JSplitPane.DIVIDER_LOCATION_PROPERTY).nn.filter(listener => listener.isInstanceOf[OnDividerChange]).foreach(listener => swing.removePropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, listener))
      Component.configuration.merge(swing, oldRepr.parent, newRepr.parent)
      swing.setDividerLocation(newRepr.dividerLocation)

      Container.resetChild(
        getChild = () => swing.getLeftComponent,
        setChild = compo => swing.setLeftComponent(compo),
        setChildWithContraint = (compo, constraint) => swing.setLeftComponent(compo),
        oldChildRepr = oldRepr.left,
        newChildRepr = newRepr.left,
      )
      Container.resetChild(
        getChild = () => swing.getRightComponent,
        setChild = compo => swing.setRightComponent(compo),
        setChildWithContraint = (compo, constraint) => swing.setRightComponent(compo),
        oldChildRepr = oldRepr.right,
        newChildRepr = newRepr.right,
      )
      swing.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, OnDividerChange(newRepr))
    }
  }

  given RenderTo[SplitPane, JSplitPane] with {
    override def getConstraints(repr: SplitPane): AnyRef | Null = repr.constraints

    override def newSwing(): JSplitPane = new JSplitPane()
  }
}
