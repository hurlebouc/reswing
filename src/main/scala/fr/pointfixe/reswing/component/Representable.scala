package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.Graphic

trait Representable[-T] {
  def render2(t : T) : Graphic
  def mergeGraphicWith2[T2 : Representable](graphic: Graphic, oldRepr: T, newRepr: T2): Graphic
}

object Representable {

  extension [T : Representable] (repr : T) {
    def render2() : Graphic = summon[Representable[T]].render2(repr)
    def mergeGraphicWith2[T2 : Representable](graphic: Graphic, newRepr: T2): Graphic = summon[Representable[T]].mergeGraphicWith2(graphic, repr, newRepr)
  }

  given confRenderRepr[RE, SW <: java.awt.Container](using renderTo: RenderTo[RE, SW], configurable: Configurable[RE, SW]): Representable[RE] with {

    def render2(repr: RE): Graphic = {
      val delegate = renderTo.newSwing()
      configurable.configure(repr, delegate)
      Graphic(delegate, renderTo.getConstraints(repr))
    }

    def mergeGraphicWith2[RE2: Representable](graphic: Graphic, oldRepr: RE, newRepr: RE2): Graphic = {
      if (oldRepr.getClass != newRepr.getClass) {
        newRepr.render2()
      } else {
        val Graphic(compo, _) = graphic
        val swing = compo.asInstanceOf[SW]
        val newContainer = newRepr.asInstanceOf[RE]
        configurable.merge(swing, oldRepr, newContainer)
        Graphic(swing, renderTo.getConstraints(newContainer))
      }
    }
  }
}

