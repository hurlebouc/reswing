package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.RenderCtx

import javax.swing.JTree
import javax.swing.event.{TreeExpansionEvent, TreeExpansionListener}
import javax.swing.tree.{DefaultTreeModel, TreeNode, TreePath}
import scala.collection.mutable

case class Tree(
                 root : TreeNode | Null = null,
                 expandedNodes : Seq[Array[Int]] = Seq(),
                 onNodeExpansion : ((Seq[Array[Int]], TreePath) => Unit) | Null = null,
                 onNodeCollapse : ((Seq[Array[Int]], TreePath) => Unit) | Null = null,
                 enabled: Boolean = true,
                 constraints: AnyRef | Null = null,
               )
{
  private val parent = new Component(
    constraints = constraints,
    enabled = enabled,
  )
}

object Tree {

  def apply(
             root : TreeNode | Null = null,
             expandedNodes : Seq[Array[Int]] = Seq(),
             onNodeExpansion : ((Seq[Array[Int]], TreePath) => Unit) | Null = null,
             onNodeCollapse : ((Seq[Array[Int]], TreePath) => Unit) | Null = null,
             enabled: Boolean = true,
             constraints: AnyRef | Null = null,
           ): Representation[Tree] = Representation(new Tree(
    root,
    expandedNodes,
    onNodeExpansion,
    onNodeCollapse,
    enabled,
    constraints,
  ))

  given configuration : Configurable[Tree, JTree] = Configurable.simpleConfigurable(_.parent, configure)

  given RenderTo[Tree, JTree] with {
    override def getConstraints(repr: Tree): AnyRef | Null = repr.constraints

    override def newSwing(): JTree = new JTree()
  }

  private case class OnNodeChange(repr : Tree, swing : JTree) extends TreeExpansionListener {
    override def treeExpanded(event: TreeExpansionEvent): Unit = {
      println(f"expend: ${getExpandedNodes(swing).map(array => Seq.from(array))}, ${event.getPath}")
      if (repr.onNodeExpansion != null) {
        repr.onNodeExpansion(getExpandedNodes(swing), event.getPath.nn)
      }
    }

    override def treeCollapsed(event: TreeExpansionEvent): Unit = {
      println(f"collapse: ${getExpandedNodes(swing).map(array => Seq.from(array))}, ${event.getPath}")
      if (repr.onNodeCollapse != null) {
        repr.onNodeCollapse(getExpandedNodes(swing), event.getPath.nn)
      }
    }
  }

  private final def configure(repr: Tree, swing: JTree) : Unit = {
    swing.getTreeExpansionListeners.nn.filter(item => item.isInstanceOf[OnNodeChange]).foreach(swing.removeTreeExpansionListener)
    if (repr.root != null) {
      swing.setModel(new DefaultTreeModel(repr.root))
    }
    repr.expandedNodes.foreach(array => swing.expandPath(Tree.getPath(array, swing.getModel.nn.getRoot.asInstanceOf)))
    swing.addTreeExpansionListener(OnNodeChange(repr, swing))
  }

  private final def getExpandedNodes(tree : JTree): Seq[Array[Int]] = {
    val expanded = tree.getExpandedDescendants(new TreePath(tree.getModel.nn.getRoot))
    if (expanded == null) {
      Seq()
    } else {
      val list: mutable.Buffer[TreePath] = mutable.Buffer()
      expanded.asIterator().nn.forEachRemaining(path => list.append(path.nn))
      list.toSeq.map(Tree.getAnonPath)
    }
  }

  private final def getAnonPath(path : TreePath) : Array[Int] = {
    val res : Array[Int] = Array.ofDim(path.getPath.nn.length - 1)
    for (i <- 0 until path.getPath.nn.length - 1) {
      val parent : TreeNode = path.getPathComponent(i).asInstanceOf
      val node : TreeNode = path.getPathComponent(i + 1).asInstanceOf
      res(i) = parent.getIndex(node)
    }
    res
  }

  private final def getPath(array : Array[Int], root : TreeNode) : TreePath = {
    val arrayRes : Array[Object | Null] = Array.ofDim(array.length + 1)
    arrayRes(0) = root
    var parent = root
    for (i <- array.indices) {
      val node = parent.getChildAt(array(i)).nn
      arrayRes(i+1) = node
      parent = node
    }
    new TreePath(arrayRes)
  }
}