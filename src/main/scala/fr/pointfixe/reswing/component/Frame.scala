package fr.pointfixe.reswing.component

import fr.pointfixe.reswing.layout.LayoutManager
import fr.pointfixe.reswingOld.Graphic

import javax.swing.{JFrame, JMenuBar, WindowConstants}

case class Frame(
             components : Seq[Representation[?]] = Seq(),
             menus : Seq[Representation[?]] = Seq(),
             constraints : AnyRef | Null = null,
             layoutManager: LayoutManager | Null = null,
             enabled : Boolean = true,
            )
{
  private val parent = new  Container(
    layoutManager = null,
    enabled = enabled,
    constraints = constraints,
  )
}

object Frame {

  def apply(components: Seq[Representation[?]] = Seq(), menus: Seq[Representation[?]] = Seq(), constraints: AnyRef | Null = null, layoutManager: LayoutManager | Null = null, enabled: Boolean = true): Representation[Frame] = Representation(new Frame(components, menus, constraints, layoutManager, enabled))

  given configuration : Configurable[Frame, JFrame] with {
    override def configure(repr: Frame, swing: JFrame): Unit = {
      Container.configuration.configure(repr.parent, swing)
      swing.setTitle("ReSwing")
      swing.setSize(800, 600)
      val contentPane = swing.getContentPane.nn
      swing.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
      if (repr.layoutManager != null) {
        contentPane.setLayout(repr.layoutManager.getSwing(swing))
      }
      for (child <- repr.components) {
        val graphic = child.render()
        if (graphic.constraints == null) {
          contentPane.add(graphic.component)
        } else {
          contentPane.add(graphic.component, graphic.constraints)
        }
      }
      val jMenuBar = new JMenuBar()
      swing.setJMenuBar(jMenuBar)
      for (menu <- repr.menus) {
        val graphic = menu.render()
        jMenuBar.add(graphic.component)
      }
      swing.setVisible(true)
    }

    override def merge(swing: JFrame, oldRepr: Frame, newRepr: Frame): Unit = {
      Container.configuration.merge(swing, oldRepr.parent, newRepr.parent)
      swing.setTitle("ReSwing")
      swing.setSize(800, 600)
      val contentPane = swing.getContentPane.nn
      swing.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
      if (newRepr.layoutManager != null) {
        contentPane.setLayout(newRepr.layoutManager.getSwing(swing))
      }
      Container.resetChildren(Children.of(contentPane), oldRepr.components, newRepr.components)
    }
  }

  given renderTo : RenderTo[Frame, JFrame] with {
    override def getConstraints(repr: Frame): AnyRef | Null = repr.constraints

    override def newSwing(): JFrame = new JFrame()
  }
}
