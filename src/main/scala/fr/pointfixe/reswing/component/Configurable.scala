package fr.pointfixe.reswing.component

trait Configurable[RE, SW] {
  def configure(repr: RE, swing: SW): Unit

  def merge(swing: SW, oldRepr: RE, newRepr: RE): Unit
}

object Configurable {

  def subConfigurable[REA, SWA, REB >: REA, SWB >: SWA] (using supConfigurable : Configurable[REB, SWB]) : Configurable[REA, SWA] = new Configurable[REA, SWA] {
    override def configure(repr: REA, swing: SWA): Unit = supConfigurable.configure(repr, swing)

    override def merge(swing: SWA, oldRepr: REA, newRepr: REA): Unit = supConfigurable.merge(swing, oldRepr, newRepr)
  }

  def parentConfigurable[REA, SWA, REB, SWB >: SWA] (getParent : REA => REB) (using supConfigurable : Configurable[REB, SWB]) : Configurable[REA, SWA] = new Configurable[REA, SWA] {
    override def configure(repr: REA, swing: SWA): Unit = supConfigurable.configure(getParent(repr), swing)

    override def merge(swing: SWA, oldRepr: REA, newRepr: REA): Unit = supConfigurable.merge(swing, getParent(oldRepr), getParent(newRepr))
  }

  def simpleConfigurable[REA, SWA, REB, SWB >: SWA](getParent: REA => REB, conf : (REA, SWA) => Unit)(using supConfigurable: Configurable[REB, SWB]): Configurable[REA, SWA] = new Configurable[REA, SWA] {
    override def configure(repr: REA, swing: SWA): Unit = {
      supConfigurable.configure(getParent(repr), swing)
      conf(repr, swing)
    }

    override def merge(swing: SWA, oldRepr: REA, newRepr: REA): Unit = {
      supConfigurable.merge(swing, getParent(oldRepr), getParent(newRepr))
      conf(newRepr, swing)
    }
  }
}
