package fr.pointfixe.reswing.component

import javax.swing.JTextField

case class TextField(
                      text : String,
                      enabled : Boolean = true,
                      constraints : AnyRef | Null = null,
                      onTextChange : String => Unit = s => {},
                    )
{
  private val parent = new TextComponent(
    text = text,
    onTextChange = onTextChange,
    enabled = enabled,
    constraints = constraints
  )
}

object TextField {

  def apply(text: String,
            enabled: Boolean = true,
            constraints: AnyRef | Null = null,
            onTextChange: String => Unit = (s : String) => {}): Representation[TextField] = Representation(new TextField(text, enabled, constraints, onTextChange))

  given configuration : Configurable[TextField, JTextField] = Configurable.parentConfigurable(_.parent)

  given renderTo : RenderTo[TextField, JTextField] with {
    override def getConstraints(repr: TextField): AnyRef | Null = repr.constraints

    override def newSwing(): JTextField = new JTextField()
  }
}
