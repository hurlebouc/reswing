package fr.pointfixe.reswing.component

import javax.swing.JMenuBar

case class MenuBar(
               menus : Seq[Representation[Menu]],
               constraints : AnyRef | Null = null,
               enabled : Boolean = true,
             )
{
  private val parent = new Component(
    enabled = enabled,
    constraints = constraints,
  )
}

object MenuBar {

  def apply(menus: Seq[Representation[Menu]],
            constraints: AnyRef | Null = null,
            enabled: Boolean = true): Representation[MenuBar] = Representation(new MenuBar(menus,
    constraints,
    enabled))

  given configuration : Configurable[MenuBar, JMenuBar] = Configurable.parentConfigurable(_.parent)
  given renderTo : RenderTo[MenuBar, JMenuBar] with {
    override def getConstraints(repr: MenuBar): AnyRef | Null = repr.constraints

    override def newSwing(): JMenuBar = new JMenuBar()
  }
}
