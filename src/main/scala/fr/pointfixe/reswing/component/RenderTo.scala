package fr.pointfixe.reswing.component

import javax.swing.JTextField

trait RenderTo[RE, SW <: java.awt.Container] {
  def getConstraints(repr : RE) : AnyRef | Null
  def newSwing() : SW

//  def render2(repr : RE) : Graphic = {
//    val delegate = newSwing()
//    configure(repr, delegate)
//    Graphic(delegate, getConstraints(repr))
//  }
//
//  def mergeGraphicWith2[RE2 : Representable](graphic: Graphic, oldRepr: RE, newRepr: RE2): Graphic = {
//    if (oldRepr.getClass != newRepr.getClass) {
//      newRepr.render2()
//    } else {
//      val Graphic(compo, _) = graphic
//      val swing = compo.asInstanceOf[SW]
//      val newContainer = newRepr.asInstanceOf[RE]
//      merge(swing, oldRepr, newContainer)
//      Graphic(swing, getConstraints(newContainer))
//    }
//  }

}

object RenderTo {

  type Truc[SW <: java.awt.Container] = [RE <: Container] =>> RenderTo[RE, SW]

  def test[A <: Container]() (using RenderTo[A, JTextField]) : Unit = {
    val truc = summon[RenderTo[A, JTextField]]
  }
}
