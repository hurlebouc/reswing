package fr.pointfixe.reswing.component
import java.awt

trait Children {
  def removeAll(): Unit

  def remove(index: Int): Unit

  def add(component: java.awt.Component, index: Int): Unit

  def add(component: java.awt.Component, constraint: AnyRef, index: Int): Unit

  def getComponents(): Seq[java.awt.Component]
}

object Children {
  def of(container : java.awt.Container) : Children = new Children{
    override def removeAll(): Unit = container.removeAll()

    override def remove(index: Int): Unit = container.remove(index)

    override def add(component: awt.Component, index: Int): Unit = container.add(component, index)

    override def add(component: awt.Component, constraint: AnyRef, index: Int): Unit = container.add(component, constraint, index)

    override def getComponents(): Seq[awt.Component] = Seq.from(container.getComponents.nn.map(compo => compo.nn))
  }

  def unique(getChild : () => java.awt.Component | Null, setChild : java.awt.Component | Null => Unit) : Children = new Children{
    override def removeAll(): Unit = setChild(null)

    override def remove(index: Int): Unit = if (index == 0) {
      setChild(null)
    } else {
      throw new RuntimeException(f"Cannot remove $index-th element of a singleton")
    }

    override def add(component: awt.Component, index: Int): Unit = if (index == 0) {
      setChild(component)
    } else {
      throw new RuntimeException(f"Cannot add $index-th element of a singleton")
    }

    override def add(component: awt.Component, constraint: AnyRef, index: Int): Unit = if (index == 0) {
      setChild(component)
    } else {
      throw new RuntimeException(f"Cannot add $index-th element of a singleton")
    }

    override def getComponents(): Seq[awt.Component] = {
      val child = getChild()
      if (child == null){
        Seq()
      } else {
        Seq(child)
      }
    }
  }
}
