package fr.pointfixe.reswing.component

import java.awt.event.{ItemEvent, ItemListener}
import javax.swing.JComboBox

case class ComboBox[E](
                        items : Seq[E] = Seq(),
                        onSelectionChange : E => Unit = (e : E) => {},
                        selectedItem : E,
                        enabled: Boolean = true,
                        constraints: AnyRef | Null = null,
                      )
{
  private val parent = new Component(
    enabled = enabled
  )
}

object ComboBox {

  private case class OnSelectionChange(repr : ComboBox[?]) extends ItemListener {
    override def itemStateChanged(e: ItemEvent): Unit = if (e.getStateChange == ItemEvent.SELECTED) {
      repr.onSelectionChange(e.getItem.asInstanceOf)
    }
  }

  private def configure[E](repr : ComboBox[E], swing : JComboBox[E]): Unit = {
    swing.getItemListeners.nn.filter(item => item.isInstanceOf[OnSelectionChange]).foreach(swing.removeItemListener)
    swing.removeAllItems()
    repr.items.foreach(swing.addItem)
    swing.setSelectedItem(repr.selectedItem)
    swing.addItemListener(OnSelectionChange(repr))
  }

  def apply[E](
                items: Seq[E],
                onSelectionChange : E => Unit = (e : E) => {},
                selectedItem : E,
                enabled: Boolean = true,
                constraints: AnyRef | Null = null): Representation[ComboBox[E]] = Representation(new ComboBox(items, onSelectionChange, selectedItem, enabled, constraints))

  given configuration[E] : Configurable[ComboBox[E], JComboBox[E]] = Configurable.simpleConfigurable(_.parent, configure)

  given renderTo[E] : RenderTo[ComboBox[E], JComboBox[E]] with {
    override def getConstraints(repr: ComboBox[E]): AnyRef | Null = repr.constraints

    override def newSwing(): JComboBox[E] = new JComboBox[E]()
  }
}