package fr.pointfixe.reswing.component

import javax.swing.{JMenu, KeyStroke}

case class Menu(
                 items : Seq[Representation[MenuItem]],
                 accelerator : Option[KeyStroke] = None,
                 text: String,
                 constraints : AnyRef | Null = null,
                 enabled : Boolean = true,
                 onClick : () => Unit = () => {},
               )
{
  private val parent = new MenuItem(
    text = text,
    onClick = onClick,
    enabled = enabled,
    accelerator = accelerator,
    constraints = constraints,
    children = items,
  )
}

object Menu {

  def apply(items: Seq[Representation[MenuItem]], accelerator: Option[KeyStroke] = None, text: String, constraints: AnyRef | Null = null, enabled: Boolean = true, onClick: () => Unit = () => {}): Representation[Menu] = Representation(new Menu(items, accelerator, text, constraints, enabled, onClick))

  given configuration : Configurable[Menu, JMenu] = Configurable.parentConfigurable(_.parent)

  given RenderTo[Menu, JMenu] with {
    override def getConstraints(repr: Menu): AnyRef | Null = repr.constraints

    override def newSwing(): JMenu = new JMenu()
  }
}
