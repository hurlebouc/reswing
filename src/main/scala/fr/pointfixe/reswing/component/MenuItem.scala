package fr.pointfixe.reswing.component

import javax.swing
import javax.swing.{JMenuItem, KeyStroke}

case class MenuItem(
                     accelerator : Option[KeyStroke] = None,
                     text: String,
                     constraints : AnyRef | Null = null,
                     enabled : Boolean = true,
                     onClick: () => Unit = () => {},
                     children: Seq[Representation[?]] | Null = null
                   )
{
  private val parent = new AbstractButton(
    text = text,
    onClick = onClick,
    enabled = enabled,
    constraints = constraints,
    children = children,
  )
}

object MenuItem {

  def apply(accelerator: Option[KeyStroke] = None,
            text: String,
            constraints: AnyRef | Null = null,
            enabled: Boolean = true,
            onClick: () => Unit = () => {},
            children: Seq[Representation[?]] | Null = null
           ): Representation[MenuItem] = Representation(new MenuItem(accelerator, text, constraints, enabled, onClick, children))

  def configure(repr: MenuItem, swing: JMenuItem): Unit = {
    //      swing.cleanAccelerators() je ne sais pas comment supprimer un accelerator
    repr.accelerator.foreach(swing.setAccelerator)
  }

  given configuration : Configurable[MenuItem, JMenuItem] = Configurable.simpleConfigurable(_.parent, configure)

  given renderTo : RenderTo[MenuItem, JMenuItem] with {
    override def getConstraints(repr: MenuItem): AnyRef | Null = repr.constraints

    override def newSwing(): JMenuItem = new JMenuItem()
  }
}