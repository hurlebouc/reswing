package fr.pointfixe.reswing.component

import fr.pointfixe.reswingOld.{Graphic, RenderCtx}

import java.awt
import java.awt.event.{FocusEvent, FocusListener}
import javax.swing.text.JTextComponent

case class TextComponent(
                          text : String,
                          onTextChange : String => Unit,
                          constraints : AnyRef | Null = null,
                          enabled : Boolean = true,
                        )
{
  private val parent = new  Component (
    enabled = enabled,
    constraints = constraints,
  )
}

object TextComponent {

  private case class OnTextChangeListener(repr : TextComponent, swing : JTextComponent) extends FocusListener {
    override def focusGained(e: FocusEvent): Unit = {}

    override def focusLost(e: FocusEvent): Unit = repr.onTextChange(swing.getText.nn)
  }

  given configuration : Configurable[TextComponent, JTextComponent] = Configurable.simpleConfigurable(_.parent, configure)

  private def configure(repr: TextComponent, swing: JTextComponent): Unit = {
    //    jTextField.getDocument.addDocumentListener(new DocumentListener {
    //      override def insertUpdate(e: DocumentEvent): Unit = onChange.foreach(c => c(jTextField.getText))
    //
    //      override def removeUpdate(e: DocumentEvent): Unit = onChange.foreach(c => c(jTextField.getText))
    //
    //      override def changedUpdate(e: DocumentEvent): Unit = onChange.foreach(c => c(jTextField.getText))
    //    })
    swing.getFocusListeners.nn.filter(item => item.isInstanceOf[OnTextChangeListener]).foreach(swing.removeFocusListener)
    swing.setText(repr.text)
    swing.addFocusListener(OnTextChangeListener(repr, swing))
  }
}
