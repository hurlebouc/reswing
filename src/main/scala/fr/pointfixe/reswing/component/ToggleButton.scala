package fr.pointfixe.reswing.component

import javax.swing.JToggleButton

case class ToggleButton(
                         selected : Boolean = false,
                         onToggleChange : Boolean => Unit = b => {},
                         constraints : AnyRef | Null = null,
                         text : String,
                         enabled : Boolean = true,
                       )
{
  private val parent = new AbstractButton(
    text = text,
    onClick = () => {onToggleChange(!selected)},
    enabled = enabled,
    constraints = constraints,
  )
}

object ToggleButton {

  def apply(selected: Boolean = false,
            onToggleChange: Boolean => Unit = b => {},
            constraints: AnyRef | Null = null,
            text: String,
            enabled: Boolean = true): Representation[ToggleButton] = Representation(new ToggleButton(selected, onToggleChange, constraints, text, enabled))

  private def configure(repr: ToggleButton, swing: JToggleButton): Unit = {
    swing.setSelected(repr.selected)
  }

  given configuration : Configurable[ToggleButton, JToggleButton] = Configurable.simpleConfigurable(_.parent, configure)

  given renderTo : RenderTo[ToggleButton, JToggleButton] with {
    override def getConstraints(repr: ToggleButton): AnyRef | Null = repr.constraints

    override def newSwing(): JToggleButton = new JToggleButton()
  }
}
