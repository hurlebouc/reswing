package fr.pointfixe.reswing.component

import fr.pointfixe.reswing.layout.LayoutManager

import javax.swing.JPanel

case class Panel (
                   children: Seq[Representation[?]],
                   layoutManager: LayoutManager | Null = null,
                   constraints : AnyRef | Null = null,
                   enabled : Boolean = true,
                 )
{
  private val parent = new  Component(
    layoutManager = layoutManager,
    enabled = enabled,
    constraints = constraints,
    children = children,
  )
}

object Panel {

  def apply(children: Seq[Representation[?]], layoutManager: LayoutManager | Null = null, constraints: AnyRef | Null = null, enabled: Boolean = true): Representation[Panel] = Representation(new Panel(children, layoutManager, constraints, enabled))

  given configuration : Configurable[Panel, JPanel] = Configurable.parentConfigurable(_.parent)

  given renderTo : RenderTo[Panel, JPanel] with {
    override def getConstraints(repr: Panel): AnyRef | Null = repr.constraints

    override def newSwing(): JPanel = new JPanel()
  }
}
