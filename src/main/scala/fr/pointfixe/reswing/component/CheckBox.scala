package fr.pointfixe.reswing.component

import javax.swing.JCheckBox

case class CheckBox(
                     selected : Boolean = false,
                     text : String = "",
                     enabled : Boolean = true,
                     constraints : AnyRef | Null = null,
                     onToggleChange : Boolean => Unit = b => {},
                   )
{
  private val parent = new ToggleButton(
    selected = selected,
    onToggleChange = onToggleChange,
    text = text,
    enabled = enabled,
    constraints = constraints,
  )
}

object CheckBox {

  def apply(selected: Boolean = false, text: String = "", enabled: Boolean = true, constraints: AnyRef | Null = null, onToggleChange: Boolean => Unit = b => {}): Representation[CheckBox] = Representation(new CheckBox(selected, text, enabled, constraints, onToggleChange))

  given configuration : Configurable[CheckBox, JCheckBox] = Configurable.parentConfigurable(_.parent)

  given renderTo : RenderTo[CheckBox, JCheckBox] with {
    override def getConstraints(repr: CheckBox): AnyRef | Null = repr.constraints

    override def newSwing(): JCheckBox = new JCheckBox()
  }
}