package fr.pointfixe.reswing

trait Future [+S] {
  def run[B](cont : S => B) : B
}

object Future {
  def ret[S](s : S) : Future[S] = new Future[S]{
    override def run[B](cont: S => B): B = cont(s)
  }
}
