package fr.pointfixe.reswing.constraint

enum BoxConstraint {
  case StickLeft, StickRight
}