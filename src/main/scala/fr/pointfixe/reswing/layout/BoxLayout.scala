package fr.pointfixe.reswing.layout
import java.awt
import java.awt.Container
import javax.swing

enum BoxLayoutType {
  case X_AXIS
  case Y_AXIS
  case LINE_AXIS
  case PAGE_AXIS
}

case class BoxLayout(boxLayoutType: BoxLayoutType) extends LayoutManager {
  override def getSwing(s: Container): awt.LayoutManager = new swing.BoxLayout(s, boxLayoutType match
    case BoxLayoutType.X_AXIS => swing.BoxLayout.X_AXIS
    case BoxLayoutType.Y_AXIS => swing.BoxLayout.Y_AXIS
    case BoxLayoutType.LINE_AXIS => swing.BoxLayout.LINE_AXIS
    case BoxLayoutType.PAGE_AXIS => swing.BoxLayout.PAGE_AXIS
  )
}
