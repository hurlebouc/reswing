package fr.pointfixe.reswing.layout

import java.awt.Container as WContainer
import java.awt.LayoutManager as JLayoutManager

trait LayoutManager {
  def getSwing(swing : WContainer) : JLayoutManager
}
