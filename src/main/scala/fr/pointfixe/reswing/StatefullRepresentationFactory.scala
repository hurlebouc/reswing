package fr.pointfixe.reswing

import fr.pointfixe.reswing.component.Representation

import scala.collection.mutable

abstract class StatefullRepresentationFactory[-A] extends RepresentationFactory[A] {


  private val values : mutable.ListBuffer[Any] = mutable.ListBuffer()
  private var currentBuild : Option[Representation[?]] = None
  protected final def useState[T] (t : T) : (() => T, T => (() => Unit) ?=> Unit) = {
    val index = values.size
    values.append(t)
    (
      () => values(index).asInstanceOf[T],

      other => redrawSignal ?=> {
        values.update(index, other)
        redrawSignal()
      })
  }

}