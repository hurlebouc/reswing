package fr.pointfixe.reswing

import fr.pointfixe.reswing.component.Representation

trait RepresentationFactory[-A] {
  def build(param : A)(using redraw : () => Unit) : Representation[?]
}