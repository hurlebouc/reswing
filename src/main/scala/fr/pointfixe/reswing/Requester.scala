package fr.pointfixe.reswing
import fr.pointfixe.reswing.component.{Button, ComboBox, Frame, Panel, Representation, ScrollPane, SplitPane, TextField, ToggleButton, Tree}
import fr.pointfixe.reswing.layout.{BoxLayout, BoxLayoutType}

import java.awt.BorderLayout

class Requester extends StatefullRepresentationFactory {

  val (getResponseBody, setResponseBody) = useState[Option[String]](None)
  val (getExpanded, setExpanded) = useState[Seq[Array[Int]]](Seq())
  private val (getUrl, setUrl) = useState("")
  private val (getMethod, setMethod) = useState("GET")
  private val (getSelected, setSelected) = useState(false)
  private val (getDivider, setDivider) = useState(200)

  override def build(param: Any)(using redraw: () => Unit): Representation[_] = Frame(
    components = Seq(
      ScrollPane(
        child = Tree(
          expandedNodes = getExpanded(),
          onNodeExpansion = (exps, newone) => setExpanded(exps),
          onNodeCollapse = (exps, newone) => setExpanded(exps),
        ),
        constraints = BorderLayout.LINE_START
      ),
      SplitPane(
        dividerLocation = getDivider(),
        onDividerLocationChange = n => setDivider(n),
        left = Panel(
          children = Seq(
            Button(
              text = getMethod()
            ),
            ComboBox(
              items = Seq("GET", "POST", "PUT", "DELETE"),
              selectedItem = getMethod(),
              onSelectionChange = m => setMethod(m),
            ),
            TextField(
              text = getUrl(),
              onTextChange = s => setUrl(s)
            ),
            ToggleButton(
              text = "select",
              selected = getSelected(),
              onToggleChange = b => setSelected(b)
            )
          ),
          layoutManager = BoxLayout(BoxLayoutType.Y_AXIS)
        ),
        right = Panel(
          children = Seq()
        ),
        constraints = BorderLayout.CENTER
      )
    )
  )
}
