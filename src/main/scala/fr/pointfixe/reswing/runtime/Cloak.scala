package fr.pointfixe.reswing.runtime

import fr.pointfixe.reswingOld.Graphic
import fr.pointfixe.reswing.RepresentationFactory
import fr.pointfixe.reswing.component.Representation

case class Cloak(factory : RepresentationFactory[Unit]) {

  private var view : Option[(Representation[?], Graphic)] = None

  private final def redraw() : Unit = {
    println("redraw")
    val newRepr = factory.build(())(using redraw)
    view match
      case Some((oldRepr, graphic)) => {
        val newGraphic = oldRepr.mergeGraphicWith(graphic, newRepr)
        val Graphic(newCompo, newConstraint) = newGraphic
//        newCompo.invalidate()
        newCompo.repaint()
        view = Some((newRepr, newGraphic))
      }
      case None => {
        view = Some(newRepr, newRepr.render())
      }
  }

  final def display(): Unit = {
    val representation = factory.build(())(using redraw)
    val graphic = representation.render()
    view = Some(representation, graphic)
  }
}
