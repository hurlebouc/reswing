package fr.pointfixe.reswingOld.widget
import fr.pointfixe.reswingOld.{RenderCtx, Widget}

import java.awt.LayoutManager
import javax.swing.JSplitPane

class SplitPane(
                 val dividerLocation: Int = 150,
                 val left: Widget,
                 val right: Widget,
                 override val layoutManager: LayoutManager | Null = null,
                 override val constraints : AnyRef | Null = null,
                 override val enabled: Boolean = true,
                 override val delegate: JSplitPane = new JSplitPane()
               )
  extends Component(
    layoutManager = layoutManager,
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    delegate.setDividerLocation(dividerLocation)
    setChild(renderCtx, g => delegate.setLeftComponent(g.component), left)
    setChild(renderCtx, g => delegate.setRightComponent(g.component), right)
  }
}
