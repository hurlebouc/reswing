package fr.pointfixe.reswingOld.widget

import javax.swing.JTextField

class TextField(
               override val text : String,
               override val enabled : Boolean = true,
               override val constraints : AnyRef | Null = null,
               override val onTextChange : String => Unit = s => {},
               override val delegate: JTextField = new JTextField(),
               )
  extends TextComponent(
    text = text,
    onTextChange = onTextChange,
    enabled = enabled,
    delegate = delegate,
    constraints = constraints
  )
