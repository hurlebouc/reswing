package fr.pointfixe.reswingOld.widget

import javax.swing.JCheckBox

case class CheckBox(
                     override val selected : Boolean = false,
                     override val text : String = "",
                     override val enabled : Boolean = true,
                     override val constraints : AnyRef | Null = null,
                     override val onToggleChange : Boolean => Unit = b => {},
                     override val delegate: JCheckBox = new JCheckBox(),
                   )
  extends ToggleButton(
    selected = selected,
    onToggleChange = onToggleChange,
    text = text,
    delegate = delegate,
    enabled = enabled,
    constraints = constraints,
  )
