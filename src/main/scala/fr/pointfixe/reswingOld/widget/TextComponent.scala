package fr.pointfixe.reswingOld.widget
import fr.pointfixe.reswingOld.RenderCtx

import java.awt.event.{FocusEvent, FocusListener}
import javax.swing.text.JTextComponent

class TextComponent (
                                            val text : String,
                                            val onTextChange : String => Unit,
                                            override val constraints : AnyRef | Null = null,
                                            override val enabled : Boolean = true,
                                            override val delegate: JTextComponent
                                          )
  extends Component (
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    delegate.setText(text)
    //    jTextField.getDocument.addDocumentListener(new DocumentListener {
    //      override def insertUpdate(e: DocumentEvent): Unit = onChange.foreach(c => c(jTextField.getText))
    //
    //      override def removeUpdate(e: DocumentEvent): Unit = onChange.foreach(c => c(jTextField.getText))
    //
    //      override def changedUpdate(e: DocumentEvent): Unit = onChange.foreach(c => c(jTextField.getText))
    //    })
    delegate.addFocusListener(new FocusListener {
      override def focusGained(e: FocusEvent): Unit = {}
      override def focusLost(e: FocusEvent): Unit = onTextChange(delegate.getText.nn)
    })
  }
}
