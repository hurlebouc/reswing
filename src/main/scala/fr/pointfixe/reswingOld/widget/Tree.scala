package fr.pointfixe.reswingOld.widget
import fr.pointfixe.reswingOld.RenderCtx

import java.awt.LayoutManager
import javax.swing.JTree
import javax.swing.event.{TreeExpansionEvent, TreeExpansionListener}
import javax.swing.tree.{DefaultTreeModel, TreeNode, TreePath}
import scala.collection.mutable

class Tree(
            val root : TreeNode | Null = null,
            val expandedNodes : Seq[Array[Int]] = Seq(),
            val onNodeExpansion : ((Seq[Array[Int]], TreePath) => Unit) | Null = null,
            val onNodeCollapse : ((Seq[Array[Int]], TreePath) => Unit) | Null = null,
            override val enabled: Boolean = true,
            override val constraints: AnyRef | Null = null,
            override val layoutManager: LayoutManager | Null = null,
            override val delegate: JTree = new JTree(),
          )
  extends Component(
    layoutManager = layoutManager,
    constraints = constraints,
    enabled = enabled,
    delegate = delegate
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    if (root != null) {
      delegate.setModel(new DefaultTreeModel(root))
    }
    expandedNodes.foreach(array => delegate.expandPath(Tree.getPath(array, delegate.getModel.nn.getRoot.asInstanceOf)))
    delegate.addTreeExpansionListener(new TreeExpansionListener {
      override def treeExpanded(event: TreeExpansionEvent): Unit = {
        println(f"expend: ${getExpandedNodes.map(array => Seq.from(array))}, ${event.getPath}")
        if (onNodeExpansion != null) {
          onNodeExpansion(getExpandedNodes, event.getPath.nn)
        }
      }

      override def treeCollapsed(event: TreeExpansionEvent): Unit = {
        println(f"collapse: ${getExpandedNodes.map(array => Seq.from(array))}, ${event.getPath}")
        if (onNodeCollapse != null) {
          onNodeCollapse(getExpandedNodes, event.getPath.nn)
        }
      }
    })
  }

  private final def getExpandedNodes : Seq[Array[Int]] = {
    val expanded = delegate.getExpandedDescendants(new TreePath(delegate.getModel.nn.getRoot))
    if (expanded == null) {
      Seq()
    } else {
      val list : mutable.Buffer[TreePath] = mutable.Buffer()
      expanded.asIterator().nn.forEachRemaining(path => list.append(path.nn))
      list.toSeq.map(Tree.getAnonPath)
    }
  }
}

object Tree {
  private final def getAnonPath(path : TreePath) : Array[Int] = {
    val res : Array[Int] = Array.ofDim(path.getPath.nn.length - 1)
    for (i <- 0 until path.getPath.nn.length - 1) {
      val parent : TreeNode = path.getPathComponent(i).asInstanceOf
      val node : TreeNode = path.getPathComponent(i + 1).asInstanceOf
      res(i) = parent.getIndex(node)
    }
    res
  }

  private final def getPath(array : Array[Int], root : TreeNode) : TreePath = {
    val arrayRes : Array[Object | Null] = Array.ofDim(array.length + 1)
    arrayRes(0) = root
    var parent = root
    for (i <- array.indices) {
      val node = parent.getChildAt(array(i)).nn
      arrayRes(i+1) = node
      parent = node
    }
    new TreePath(arrayRes)
  }
}