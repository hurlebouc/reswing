package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.{RenderCtx, Widget}

import java.awt.LayoutManager
import javax.swing.{BoxLayout, JPanel}

class Panel (
             val children: Seq[Widget],
             override val layoutManager: LayoutManager | Null = null,
             override val constraints : AnyRef | Null = null,
             override val enabled : Boolean = true,
             override val delegate: JPanel = new JPanel()
            )
  extends Component (
    layoutManager = layoutManager,
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{

  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    setChildren(renderCtx, children)
  }

  override def adaptWith(widget: Widget): Widget = {
    if (this.getClass == widget.getClass) {
      val newChildrenTemp = children.zip(widget.asInstanceOf[Panel].children).map((older, newer) => older.adaptWith(newer))
      val newlength = newChildrenTemp.length
      val newChildren = newChildrenTemp ++ widget.asInstanceOf[Panel].children.drop(newlength)
      new Panel(
        children = newChildren,
        layoutManager, constraints, enabled, delegate
      )
    } else {
      widget
    }
  }
}