package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.{Graphic, RenderCtx, Widget}

import java.awt.{Component, LayoutManager, Container as WContainer}


class Container (
                  val layoutManager: LayoutManager | Null = null,
                  val constraints : AnyRef | Null = null,
                  val enabled : Boolean,
                  val delegate : WContainer
               )
  extends Widget
{

  def configure(renderCtx: RenderCtx) : Unit = {
    delegate.setEnabled(enabled)
    if (layoutManager != null) {
      delegate.setLayout(layoutManager)
    }
  }

  override final def render(renderCtx: RenderCtx): Graphic = {
    configure(renderCtx)
    Graphic(delegate, constraints)
  }

  protected final def setChildren(renderCtx: RenderCtx, children : Seq[Widget]) : Unit = {
    for (i <- children.indices) {
      setChild(
        renderCtx = renderCtx,
        child = children(i),
        setter = graphic => {
          if (i < delegate.getComponentCount) {
            delegate.remove(i)
          }
          delegate.add(graphic.component, graphic.constraints, i)
        },
      )
    }
  }

  protected final def setChild(renderCtx: RenderCtx, setter: Graphic => Unit, child: Widget): Unit = {
    val rx = renderCtx.copy(
      refresh = graphic => {
        setter(graphic)
        renderCtx.refresh(Graphic(delegate, constraints))
        delegate.invalidate()
        delegate.repaint()
      }
    )
    setter(child.render(rx))
  }
}

object Container{
}