package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.{RenderCtx, Widget}

import java.awt.LayoutManager
import javax.swing.JScrollPane

class ScrollPane(
                  val child : Widget,
                  override val enabled: Boolean = true,
                  override val constraints: AnyRef | Null = null,
                  override val layoutManager: LayoutManager | Null = null,
                  override val delegate: JScrollPane = new JScrollPane(),
          )
  extends Component(
    layoutManager = layoutManager,
    constraints = constraints,
    enabled = enabled,
    delegate = delegate
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    setChild(renderCtx, graphic => delegate.getViewport.nn.setView(graphic.component), child)
  }
}
