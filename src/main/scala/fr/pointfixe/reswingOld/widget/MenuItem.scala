package fr.pointfixe.reswingOld.widget
import fr.pointfixe.reswingOld.RenderCtx

import javax.swing
import javax.swing.{JMenuItem, KeyStroke}

class MenuItem(
                val accelerator : Option[KeyStroke] = None,
                override val text: String,
                override val constraints : AnyRef | Null = null,
                override val enabled : Boolean = true,
                override val onClick: () => Unit = () => {},
                override val delegate: JMenuItem = new JMenuItem(),
              )
  extends AbstractButton(
    text = text,
    onClick = onClick,
    delegate = delegate,
    enabled = enabled,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    accelerator.foreach(delegate.setAccelerator)
  }
}

object MenuItem {

}