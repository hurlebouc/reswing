package fr.pointfixe.reswingOld.widget
import fr.pointfixe.reswingOld.RenderCtx

import java.awt.LayoutManager
import javax.swing.{JMenu, JMenuBar}

class MenuBar(
               val menus : Seq[Menu],
               override val layoutManager: LayoutManager | Null = null,
               override val constraints : AnyRef | Null = null,
               override val enabled : Boolean = true,
               override val delegate: JMenuBar = new JMenuBar()
             )
  extends Component(
    layoutManager = layoutManager,
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    setChildren(renderCtx, menus)
  }
}
