package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.RenderCtx

import javax.swing.JLabel

class Label(
                           text : String,
                           override val constraints : AnyRef | Null = null,
                           override val enabled : Boolean = true,
                           override val delegate: JLabel = new JLabel()
                         )
  extends Component(
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    delegate.setText(text)
  }
}
