package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.RenderCtx

import javax.swing.AbstractButton as JAbstractButton

class AbstractButton(
                      val text : String,
                      val onClick : () => Unit,
                      override val constraints : AnyRef | Null = null,
                      override val enabled : Boolean,
                      override val delegate: JAbstractButton
                    )
  extends Component(
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{

  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    delegate.setText(text)
    delegate.addActionListener(actionEvent => onClick())
    delegate.setEnabled(enabled)
  }
}
