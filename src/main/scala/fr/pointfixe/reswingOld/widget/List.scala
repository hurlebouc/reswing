package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.RenderCtx

import java.awt.LayoutManager
import java.util
import javax.swing.JList

class List[E](
               val items : Seq[E],
               override val enabled: Boolean = true,
               override val constraints: AnyRef | Null = null,
               override val layoutManager: LayoutManager | Null = null,
               override val delegate: JList[E] = new JList[E](),
          )
  extends Component(
    layoutManager = layoutManager,
    constraints = constraints,
    enabled = enabled,
    delegate = delegate
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    import scala.jdk.CollectionConverters.*
    super.configure(renderCtx)
    delegate.setListData(new util.Vector(items.asJava))
  }
}
