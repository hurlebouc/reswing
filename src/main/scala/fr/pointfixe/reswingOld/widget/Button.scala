package fr.pointfixe.reswingOld.widget

import javax.swing.JButton

class Button(
              override val text : String,
              override val enabled : Boolean = true,
              override val onClick : () => Unit = () => {},
              override val constraints : AnyRef | Null = null,
              override val delegate : JButton = new JButton()
            )
  extends AbstractButton(
    text = text,
    onClick = onClick,
    delegate = delegate,
    enabled = enabled,
    constraints = constraints,
  )