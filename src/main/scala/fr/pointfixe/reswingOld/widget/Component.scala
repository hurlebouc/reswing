package fr.pointfixe.reswingOld.widget

import java.awt.LayoutManager
import javax.swing.JComponent


class Component (
                  override val layoutManager: LayoutManager | Null = null,
                  override val constraints : AnyRef | Null = null,
                  override val enabled : Boolean,
                  override val delegate : JComponent
                )
  extends Container (
    layoutManager = layoutManager,
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )