package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.{Graphic, RenderCtx, Widget}

import java.awt.{FlowLayout, LayoutManager, Container as WContainer}
import javax.swing.{JFrame, JMenuBar, JPanel, WindowConstants}

class Frame (
              val children : Seq[Widget] = Seq(),
              val menus : Seq[Widget] = Seq(),
              override val constraints : AnyRef | Null = null,
              override val layoutManager: LayoutManager | Null = null,
              override val enabled : Boolean = true,
              override val delegate: JFrame = new JFrame()
            )
  extends Container(
    layoutManager = layoutManager,
    enabled = enabled,
    delegate = delegate,
    constraints = constraints,
  )
{


  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    delegate.setTitle("ReSwing")
    delegate.setSize(800, 600)
    val contentPane = delegate.getContentPane.nn
    delegate.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
    if (layoutManager != null) {
      contentPane.setLayout(layoutManager)
    }
    setChildren(renderCtx, contentPane, children)
    val jMenuBar = new JMenuBar()
    delegate.setJMenuBar(jMenuBar)
    setChildren(renderCtx, jMenuBar, menus)
    delegate.setVisible(true)
  }

  private[this] def setChildren(renderCtx: RenderCtx, container : WContainer, children : Seq[Widget]) : Unit = {
    for (i <- children.indices) {
      val rx = renderCtx.copy(
        refresh = graphic => {
          container.remove(i)
          container.add(graphic.component, graphic.constraints, i)
          renderCtx.refresh(Graphic(delegate, constraints))
          container.revalidate()
          container.repaint()
        }
      )
      val graphic = children(i).render(rx)
      container.add(graphic.component, graphic.constraints)
    }
  }
}
