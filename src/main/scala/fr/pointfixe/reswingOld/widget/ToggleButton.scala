package fr.pointfixe.reswingOld.widget

import fr.pointfixe.reswingOld.RenderCtx

import javax.swing.JToggleButton

class ToggleButton(
                    val selected : Boolean = false,
                    val onToggleChange : Boolean => Unit = b => {},
                    override val constraints : AnyRef | Null = null,
                    override val text : String,
                    override val enabled : Boolean = true,
                    override val delegate: JToggleButton = new JToggleButton()
                  )
  extends AbstractButton(
    text = text,
    onClick = () => {onToggleChange(delegate.isSelected)},
    delegate = delegate,
    enabled = enabled,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    delegate.setSelected(selected)
  }
}
