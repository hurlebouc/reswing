package fr.pointfixe.reswingOld.widget
import fr.pointfixe.reswingOld.RenderCtx

import javax.swing.{JMenu, JMenuItem, KeyStroke}

class Menu(
                         val items : Seq[MenuItem],
                         override val accelerator : Option[KeyStroke] = None,
                         override val text: String,
                         override val constraints : AnyRef | Null = null,
                         override val enabled : Boolean = true,
                         override val onClick : () => Unit = () => {},
                         override val delegate: JMenu = new JMenu()
          )
  extends MenuItem(
    text = text,
    onClick = onClick,
    delegate = delegate,
    enabled = enabled,
    accelerator = accelerator,
    constraints = constraints,
  )
{
  override def configure(renderCtx: RenderCtx): Unit = {
    super.configure(renderCtx)
    setChildren(renderCtx, items)
  }
}
