package fr.pointfixe.reswingOld

trait Widget {
  def render(renderCtx: RenderCtx) : Graphic

  /**
   * Fonction renvoyant le widget courant adapté au nouveau widget.
   * Le comportement par défaut est de toujours renvoyer le nouveau widget.
   * @param widget
   * @return
   */
  def adaptWith(widget: Widget) : Widget = widget

  def run() : Unit = {
    val renderCtx = RenderCtx(component => {})
    render(renderCtx)
  }

}
