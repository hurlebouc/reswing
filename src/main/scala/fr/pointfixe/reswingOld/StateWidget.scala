package fr.pointfixe.reswingOld

import scala.collection.mutable

trait StateWidget extends Widget{

  def build(renderCtx: RenderCtx) : Widget

  private var values : mutable.ListBuffer[Any] = mutable.ListBuffer()
  private var currentBuild : Option[Widget] = None
  protected final def useState[T] (t : T) : (() => T, T => RenderCtx ?=> Unit) = {
    val index = values.size
    values.append(t)
    (
      () => values(index).asInstanceOf[T],

      other => renderCtx ?=> {
        values.update(index, other)
        renderCtx.refresh(render(renderCtx))
      })
  }

  /**
   * Méthode à surcharger pour les composants ayant un lien direct avec Swing
   *
   * @param renderCtx contexte de construction du widget parent
   * @return
   */
  def render(renderCtx: RenderCtx): Graphic = {
    println(s"Widget.render($renderCtx)")
    val newBuild = build(renderCtx)
    val mergedBuild = currentBuild match {
      case Some(previousBuild) => previousBuild.adaptWith(newBuild)
      case None => newBuild
    }
    currentBuild = Some(mergedBuild)
    mergedBuild.render(renderCtx)
  }

  override def adaptWith(widget: Widget): Widget = {
    println("adaptWith")
    if (this.getClass == widget.getClass) {
      widget.asInstanceOf[StateWidget].values = this.values
      widget.asInstanceOf[StateWidget].currentBuild = this.currentBuild
    }
    widget
  }
}