package fr.pointfixe.reswingOld

import fr.pointfixe.reswingOld.widget.*
import fr.pointfixe.reswingOld.widget.List as RList

import java.awt.BorderLayout
import javax.swing.*
import javax.swing.tree.TreePath

@main def hello(): Unit = {
  Frame(
    children = Seq(new StateWidget{
      val (getCounter, setCounter) = useState(0)
      val (getMessage, setMessage) = useState("Coucou")
      val (getList, setList) = useState[Seq[String]](Seq())
      val (getExpanded, setExpanded) = useState[Seq[Array[Int]]](Seq())

      override def build(using renderCtx: RenderCtx): Widget = {
        SplitPane(
          dividerLocation = 150,
          constraints = BorderLayout.CENTER,
          left = ScrollPane(
            //child = RList[String](getList())
            child = Tree(
              expandedNodes = getExpanded(),
              onNodeExpansion = (exps, newone) => setExpanded(exps),
              onNodeCollapse = (exps, newone) => setExpanded(exps),
            )
          ),
          right = Panel(children = Seq(
            Button(s"${getMessage()} ${getCounter()}", onClick = () => setCounter(getCounter() + 1)),
            TextField(s"${getMessage()}", onTextChange = text => setMessage(text)),
            Button("coucou", onClick = () => setList(getList().appended("coucou"))),
          )),
        )
      }
    }),
    //layoutManager = new BorderLayout(),
  ).run()
}

@main def globalState() : Unit = {
  new StateWidget {
    val (getCounter, setCounter) = useState(0)
    val (getMessage, setMessage) = useState("Coucou")
    val (getList, setList) = useState[Seq[String]](Seq())

    override def build(using renderCtx: RenderCtx): Widget = Frame(
      children = Seq(
        SplitPane(
          constraints = BorderLayout.WEST,
          left = Button("coucou"),
          right = Panel(children = Seq(
            Button(s"${getMessage()} ${getCounter()}", onClick = () => setCounter(getCounter() + 1)),
            TextField(s"${getMessage()}", onTextChange = text => setMessage(text)),
          )),
        )
      )
    )
  }.run()
}

def incrementalCounter() : Widget = new StateWidget{
  println("Create new incrementalCounter")
  val (getCounter, setCounter) = useState(0)

  override def build(using renderCtx: RenderCtx): Widget = {
    println(f"incrementalCounter : ${getCounter()}")
    Button(
      text = s"coucou ${getCounter()}",
      onClick = () => setCounter(getCounter() + 1),
    )
  }
}

def nestedWidget() : Widget = new StateWidget{

  val (getText, setText) = useState("coucou")

  override def build(using renderCtx: RenderCtx): Widget = Panel(children = Seq(
    TextField(text = getText(), onTextChange = s => {setText(s)}),
    Button(text = getText()),
    incrementalCounter(),
  ))
}

@main def testNested() : Unit = { // TODO : problème ici avec les éléments nested
  Frame(children = Seq(
    nestedWidget()
  )).run()
}

@main def testEqClass(): Unit = {
  val o1 = new Object {}
  val o2 = new Object {}
  println(o1.getClass)
  println(o2.getClass)
  println(o1.getClass == o2.getClass)
}

@main def helloOld(): Unit = {
  val menuitem = new JMenuItem()
  menuitem.setText("coucou")
  menuitem.setAccelerator(KeyStroke.getKeyStroke("ctrl O"))
  menuitem.addActionListener(e => println("coucou"))
  val menu = new JMenu()
  menu.setText("Plop")
  menu.add(menuitem)
  val menubar = new JMenuBar()
  menubar.add(menu)
  val jFrame = new JFrame()
  jFrame.setJMenuBar(menubar)
  jFrame.setTitle("ReSwing")
  jFrame.setSize(800, 600)
  val contentPane = jFrame.getContentPane.asInstanceOf[JPanel]
  jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

  // button
  val jButton = new JButton("coucou 1")
  jButton.addActionListener(e => {
    contentPane.remove(0)
    val jButton2 = new JButton("coucou 2")
    contentPane.add(jButton2, BorderLayout.EAST, 0)
    contentPane.revalidate()
    contentPane.repaint()
  })
  contentPane.add(jButton, BorderLayout.EAST)

  contentPane.add(new JButton("button 2"), BorderLayout.WEST)
  println(contentPane)
  println(contentPane.getComponentCount)
//  contentPane.remove(1)

  // splitpane
  val splitPane = new JSplitPane()
  splitPane.setLeftComponent(new JButton("button left"))
  splitPane.setRightComponent(new JButton("button right"))
  splitPane.setDividerLocation(0.5)
  contentPane.add(splitPane)

  jFrame.setVisible(true)
  //splitPane.setDividerLocation(0.5)
}
