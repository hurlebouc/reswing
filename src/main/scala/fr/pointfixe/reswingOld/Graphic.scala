package fr.pointfixe.reswingOld

import java.awt.Component as WComponent

/**
 * Classe représentant le contenu graphique cible, c'était à dire la représentation en Swing
 * @param component
 * @param constraints
 */
case class Graphic(component: WComponent, constraints : AnyRef | Null)
